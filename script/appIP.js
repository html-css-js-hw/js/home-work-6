async function findLocation() {
    try {
        const ipAddress = await getIpAddress();
        const locationInfo = await getLocationInfo(ipAddress);
        displayLocationInfo(locationInfo);
    } catch (error) {
        console.error('Помилка:', error);
    }
}

async function getIpAddress() {
    const response = await fetch('https://api.ipify.org/?format=json');
    const data = await response.json();
    return data.ip;
}

async function getLocationInfo(ipAddress) {
    const response = await fetch(`https://ip-api.com/json/${ipAddress}?fields=continent,country,regionName,city,district`);
    const data = await response.json();
    return {
        continent: data.continent,
        country: data.country,
        region: data.regionName,
        city: data.city,
        district: data.district,
    };
}

function displayLocationInfo(locationInfo) {
    const locationDiv = document.getElementById('locationInfo');
    locationDiv.innerHTML = `
    <p>Континент: ${locationInfo.continent}</p>
    <p>Країна: ${locationInfo.country}</p>
    <p>Регіон: ${locationInfo.region}</p>
    <p>Місто: ${locationInfo.city}</p>
    <p>Район: ${locationInfo.district}</p>
  `;
}